package io.gitlab.hjoeren.liquibase.cdi.extension;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class wrapping the properties to setup Liquibase and executing a Liquibase update.
 * 
 * @author Joeren Haag
 */
public class LiquibaseProperties {

  private String changeLogFile;
  private String url;
  private String username;
  private String password;
  private String contexts;
  private String defaultSchema;
  private boolean shouldRun = true;

  public LiquibaseProperties() {}

  /**
   * Creates a {@code LiquibaseProperties} object with the specific properties.
   * 
   * @param changeLogFile the path of the change log file
   * @param url the JDBC URL
   * @param contexts the contexts
   * @param defaultSchema the default schema
   * @param shouldRun whether the Liquibase CDI extension should run or not
   */
  public LiquibaseProperties(String changeLogFile, String url, String username, String password,
      String contexts, String defaultSchema, boolean shouldRun) {
    this.changeLogFile = changeLogFile;
    this.url = url;
    this.username = username;
    this.password = password;
    this.contexts = contexts;
    this.defaultSchema = defaultSchema;
    this.shouldRun = shouldRun;
  }

  /**
   * Creates a {@code LiquibaseProperties} object with loading the properties with the specific
   * properties path.
   * 
   * @param propertiesPath the path of the properties
   */
  public LiquibaseProperties(String propertiesPath) {
    Properties properties = new Properties();
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    InputStream stream = classLoader.getResourceAsStream("liquibase.properties");
    try {
      properties.load(stream);
    } catch (IOException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
    loadFromProperties(properties);
  }

  /**
   * Creates a {@code LiquibaseProperties} object with the specific properties.
   * 
   * @param properties the properties
   */
  public LiquibaseProperties(Properties properties) {
    loadFromProperties(properties);
  }

  /**
   * Loads the specific properties. Existing properties are overwritten.
   * 
   * @param properties the properties
   */
  public void loadFromProperties(Properties properties) {
    changeLogFile = properties.getProperty(LiquibasePropertyKey.CHANGE_LOG_FILE.getKey());
    url = properties.getProperty(LiquibasePropertyKey.URL.getKey());
    username = properties.getProperty(LiquibasePropertyKey.USERNAME.getKey());
    password = properties.getProperty(LiquibasePropertyKey.PASSWORD.getKey());
    contexts = properties.getProperty(LiquibasePropertyKey.CONTEXTS.getKey());
    defaultSchema = properties.getProperty(LiquibasePropertyKey.DEFAULT_SCHEMA.getKey());
    String shouldRunString = properties.getProperty(LiquibasePropertyKey.SHOULD_RUN.getKey());
    shouldRun = Boolean.parseBoolean(shouldRunString != null ? shouldRunString : "true");
  }

  public String getChangeLogFile() {
    return changeLogFile;
  }

  public void setChangeLogFile(String changeLogFile) {
    this.changeLogFile = changeLogFile;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getContexts() {
    return contexts;
  }

  public void setContexts(String contexts) {
    this.contexts = contexts;
  }

  public String getDefaultSchema() {
    return defaultSchema;
  }

  public void setDefaultSchema(String defaultSchema) {
    this.defaultSchema = defaultSchema;
  }

  public boolean isShouldRun() {
    return shouldRun;
  }

  public void setShouldRun(boolean shouldRun) {
    this.shouldRun = shouldRun;
  }

}
