package io.gitlab.hjoeren.liquibase.cdi.extension.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Qualifier;

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
public @interface LiquibaseType {

  public final static class Literal extends AnnotationLiteral<LiquibaseType>
      implements LiquibaseType {

    public static final Literal INSTANCE = new Literal();

    private static final long serialVersionUID = 1L;

  }

}
