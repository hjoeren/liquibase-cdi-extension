package io.gitlab.hjoeren.liquibase.cdi.extension;

/**
 * Enum for the Liquibase properties keys.
 * 
 * @author Joeren Haag
 */
public enum LiquibasePropertyKey {

  CHANGE_LOG_FILE("changeLogFile"), URL("url"), USERNAME("username"), PASSWORD(
      "password"), CONTEXTS("contexts"), DEFAULT_SCHEMA("defaultSchema"), SHOULD_RUN("shouldRun");

  private String key;

  private LiquibasePropertyKey(String key) {
    this.key = key;
  }

  public String getKey() {
    return key;
  }

}
