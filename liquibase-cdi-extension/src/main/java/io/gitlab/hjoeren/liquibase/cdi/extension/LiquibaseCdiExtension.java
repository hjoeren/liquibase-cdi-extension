package io.gitlab.hjoeren.liquibase.cdi.extension;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import io.gitlab.hjoeren.liquibase.cdi.extension.annotation.LiquibaseType;
import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.configuration.GlobalConfiguration;
import liquibase.configuration.LiquibaseConfiguration;
import liquibase.database.DatabaseConnection;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ResourceAccessor;

/**
 * The {@code LiquibaseCdiExtension} class is responsible to run Liquibase on each deploy.
 * 
 * @author Joeren Haag
 */
@ApplicationScoped
public class LiquibaseCdiExtension {

  private static final Logger LOG = Logger.getLogger(LiquibaseCdiExtension.class.getSimpleName());

  @Inject
  @LiquibaseType
  ResourceAccessor resourceAccessor;
  @Inject
  @LiquibaseType
  LiquibaseProperties liquibaseProperties;

  /**
   * Observes the initialization of the {@link ApplicationScoped}.
   * 
   * @param event
   */
  void onApplicationScopeInitialized(@Observes @Initialized(ApplicationScoped.class) Object event) {
    GlobalConfiguration configuration =
        LiquibaseConfiguration.getInstance().getConfiguration(GlobalConfiguration.class);
    if (!configuration.getShouldRun()) {
      LOG.info("Skipping execute liquibase update because of global configuration");
    } else if (!liquibaseProperties.isShouldRun()) {
      LOG.info("Skipping execute liquibase update because of liquibase properties");
    } else {
      executeLiquibaseUpdate(liquibaseProperties, resourceAccessor);
    }
  }

  /**
   * Executes a Liquibase update with the specific properties.
   * 
   * @param liquibaseProperties the properties to use for the Liquibase update
   */
  public void executeLiquibaseUpdate(LiquibaseProperties liquibaseProperties,
      ResourceAccessor resourceAccessor) {
    String url = liquibaseProperties.getUrl();
    String username = liquibaseProperties.getUsername();
    String password = liquibaseProperties.getPassword();
    String changeLogFile = liquibaseProperties.getChangeLogFile();
    Contexts contexts = new Contexts(liquibaseProperties.getContexts());
    String defaultSchema = liquibaseProperties.getDefaultSchema();
    Connection c = null;
    DatabaseConnection conn = null;
    try {
      if (username == null) {
        c = DriverManager.getConnection(url);
      } else {
        c = DriverManager.getConnection(url, username, password);
      }
      conn = new JdbcConnection(c);
      try (Liquibase liquibase = new Liquibase(changeLogFile, resourceAccessor, conn)) {
        if (defaultSchema != null) {
          liquibase.getDatabase().setDefaultSchemaName(defaultSchema);
        }
        LOG.info("Executing liquibase update");
        liquibase.update(contexts);
        LOG.info("Executed liquibase update");
      } catch (Exception e1) {
        throw new RuntimeException(e1.getMessage(), e1);
      }
    } catch (SQLException e) {
      throw new RuntimeException(e.getMessage(), e);
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (DatabaseException e) {
          LOG.warning(e.getMessage());
        }
      } else if (c != null) {
        try {
          c.close();
        } catch (SQLException e) {
          LOG.warning(e.getMessage());
        }
      }
    }
  }

}
