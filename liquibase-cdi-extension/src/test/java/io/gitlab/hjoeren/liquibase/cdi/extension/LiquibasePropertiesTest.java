package io.gitlab.hjoeren.liquibase.cdi.extension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class LiquibasePropertiesTest {

  @Test
  void testLiquibasePropertiesWithPropertiesPath() {
    LiquibaseProperties liquibaseProperties = new LiquibaseProperties("liquibase.properties");
    assertEquals("./changelog.xml", liquibaseProperties.getChangeLogFile());
    assertEquals("jdbc:h2:mem:lcdi;DB_CLOSE_DELAY=-1", liquibaseProperties.getUrl());
    assertEquals("SA", liquibaseProperties.getUsername());
    assertEquals("SA", liquibaseProperties.getPassword());
    assertEquals("all", liquibaseProperties.getContexts());
    assertEquals(true, liquibaseProperties.isShouldRun());
  }

}
