package io.gitlab.hjoeren.liquibase.cdi.extension;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import io.gitlab.hjoeren.liquibase.cdi.extension.annotation.LiquibaseType;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;

public class LiquibaseProducers {

  @Produces
  @ApplicationScoped
  @LiquibaseType
  public LiquibaseProperties createLiquibaseProperties() {
    return new LiquibaseProperties("liquibase.properties");
  }

  @Produces
  @ApplicationScoped
  @LiquibaseType
  public ResourceAccessor createResourceAccessor() {
    return new ClassLoaderResourceAccessor(getClass().getClassLoader());
  }

}
