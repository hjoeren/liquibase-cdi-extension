package io.gitlab.hjoeren.liquibase.cdi.extension;

import static org.junit.jupiter.api.Assertions.assertTrue;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.enterprise.inject.se.SeContainer;
import javax.enterprise.inject.se.SeContainerInitializer;
import javax.enterprise.inject.spi.CDI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import io.gitlab.hjoeren.liquibase.cdi.extension.annotation.LiquibaseType;

class LiquibaseCdiExtensionIT {

  SeContainer container;
  LiquibaseProperties properties;

  @BeforeEach
  void setUp() {
    SeContainerInitializer containerInitializer = SeContainerInitializer.newInstance();
    container = containerInitializer.addBeanClasses(LiquibaseProducers.class).initialize();
    properties = CDI.current().select(LiquibaseProperties.class, LiquibaseType.Literal.INSTANCE).get();
  }

  @Test
  void testLiquibaseCdiExtension() throws SQLException {
    String url = properties.getUrl();
    String username = properties.getUsername();
    String password = properties.getPassword();
    Connection c;
    if (username == null) {
      c = DriverManager.getConnection(properties.getUrl());
    } else {
      c = DriverManager.getConnection(url, username, password);
    }
    DatabaseMetaData metaData = c.getMetaData();
    ResultSet tables = metaData.getTables(null, null, "SIMPLE_TABLE", null);
    assertTrue(tables.next());
  }

}
