package io.gitlab.hjoeren.liquibase.cdi.extension.usage;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import io.gitlab.hjoeren.liquibase.cdi.extension.LiquibaseProperties;
import io.gitlab.hjoeren.liquibase.cdi.extension.annotation.LiquibaseType;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;

/**
 * Class that provides the producers to run the Liquibase CDI extension.
 * 
 * @author Joeren Haag
 */
public class LiquibaseProducers {

  // Option 1: Use a properties file

  @Produces
  @ApplicationScoped
  @LiquibaseType
  public LiquibaseProperties createLiquibaseProperties() {
    return new LiquibaseProperties("liquibase.properties");
  }

  // Option 2: Use setters of LiquibaseProperties

  // @Produces
  // @ApplicationScoped
  // @LiquibaseType
  // public LiquibaseProperties createLiquibaseProperties() {
  // LiquibaseProperties liquibaseProperties = new LiquibaseProperties();
  // liquibaseProperties.setChangeLogFile("./changelog.xml");
  // liquibaseProperties.setUrl("jdbc:h2:~/lcdi/lcdi;AUTO_SERVER=true");
  // return liquibaseProperties;
  // }

  @Produces
  @ApplicationScoped
  @LiquibaseType
  public ResourceAccessor createResourceAccessor() {
    return new ClassLoaderResourceAccessor(getClass().getClassLoader());
  }

}
