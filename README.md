# Liquibase CDI extension

Alternative to the Liquibase CDI module[^lcdim].

## Differences to Liquibase CDI module

 - (Besides a `ResourceAccessor` bean) Only provide a `LiquibaseProperties` bean that itself provides
    - the change log file (required),
    - the URL of the database connection (required),
    - the contexts (optional) and
    - a flag whether a Liquibase update should be executed or not (optional, default `true`)
 - The "extension" (it is no really CDI extension) observes the initialization of the application scope and then constructs a `Liquibase` object with the provided properties and executes an update

## Modules

The project consists of two separate Maven projects:

 1. `liquibase-cdi-extension`: the extension itself
 2. `liquibase-cdi-extension-usage`: a simple web application making use of the extension

## Usage (example)

### Clone and install the project

```
git clone git@gitlab.com:hjoeren/liquibase-cdi-extension.git
cd liquibase-cdi-extension
mvn install
```

### Add a dependency to the extension module

```
<dependency>
  <groupId>io.gitlab.hjoeren</groupId>
  <artifactId>liquibase-cdi-extension</artifactId>
  <version>${liquibase-cdi-extension.version}</version>
</dependency>
```

### Create a producer for `ResourceAccessor`

```
@Produces                                                             
@ApplicationScoped                                                    
@LiquibaseType                                                        
public ResourceAccessor createResourceAccessor() {                    
  return new ClassLoaderResourceAccessor(getClass().getClassLoader());
}                                                                     
```

### Create a producer for `LiquibaseProperties`

> `changelog.xml` is located under `src/main/resources/`

#### Option 1: Use a properties file

Using a properties file in addition with Maven filtering and Maven profiles provides a flexible way to define different URLs and contexts for different environments.

##### Create a properties file

> `liquibase.properties` is located under `src/main/resources/`

```
changeLogFile=./changelog.xml
url=jdbc:h2:~/lcdi/lcdi;AUTO_SERVER=true
```

##### Create the producer

```
@Produces                                                                  
@ApplicationScoped                                                         
@LiquibaseType                                                             
public LiquibaseProperties createLiquibaseProperties() {                   
  return new LiquibaseProperties("liquibase.properties");                                              
}                                                                          
```

#### Option 2: Use setters of `LiquibaseProperties`

There is also an extra constructor to set all properties.

```
@Produces                                                              
@ApplicationScoped                                                     
@LiquibaseType                                                         
public LiquibaseProperties createLiquibaseProperties() {               
  LiquibaseProperties liquibaseProperties = new LiquibaseProperties(); 
  liquibaseProperties.setChangeLogFile("./changelog.xml");             
  liquibaseProperties.setUrl("jdbc:h2:~/lcdi/lcdi;AUTO_SERVER=true");  
  return liquibaseProperties;                                          
}                                                                      
```

## Resources

[^lcdim]: "Liquibase CDI module sources on GitHub". https://github.com/liquibase/liquibase/tree/master/liquibase-cdi